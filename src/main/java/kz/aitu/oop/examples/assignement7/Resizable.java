package kz.aitu.oop.examples.assignement7;

public interface Resizable  {
    public void resizeDec(int percent);
    public void resizeInc(int percent);
}
