package kz.aitu.oop.examples.assignement7;

import org.omg.CORBA.PRIVATE_MEMBER;

public class MovablePoint implements Movable{
    int x;
    int y;
    int xSpeed;
    int ySpeed;

    public MovablePoint(int x, int y, int xSpeed, int ySpeed){
        this.x = x;
        this.y = y;
        this.xSpeed = xSpeed;
        this.ySpeed = ySpeed;
    }

    @Override
    public String toString() {
        return "MovablePoint{" +
                "x=" + x +
                ", y=" + y +
                ", xSpeed=" + xSpeed +
                ", ySpeed=" + ySpeed +
                '}';
    }
    @Override
    public void moveUp(){
        ySpeed++;
    }
    @Override
    public void moveDown(){
        ySpeed--;
    }
    @Override
    public void moveLeft(){
        xSpeed--;
    }
    @Override
    public void moveRight(){
        xSpeed++;
    }

}
