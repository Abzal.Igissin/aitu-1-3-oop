package kz.aitu.oop.examples.assignement7;

public class Circle1 implements GeometricObject {
    protected double radius = 1.0;

    public Circle1(double radius){
        this.radius = radius;
    }

    @Override
    public String toString() {
        return "Circle1{" +
                "radius=" + radius +
                '}';
    }
    @Override
public double getPerimeter(){
        return 2 * Math.PI*radius;
    }
    @Override
    public double getArea(){
        return Math.PI*radius*radius;
    }
}
