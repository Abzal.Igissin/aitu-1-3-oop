package kz.aitu.oop.examples.assignement7;

public abstract class ResizableCircle extends Circle1 implements Resizable{
    public ResizableCircle(double radius){
    super(radius);
    }

    @Override
    public String toString() {
        return super.toString();
    }
    @Override
    public void resizeDec(int percent){
        radius = radius - (radius * (percent / 100));
    }
    @Override
    public void resizeInc(int percent){
        radius = radius + (radius * (percent/100));
    }

}
