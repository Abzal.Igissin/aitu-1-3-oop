package kz.aitu.oop.examples.assignement7;

public abstract class MovableCircle implements Movable {

    private int radius;
    private MovablePoint center;

    public MovableCircle(int x, int y, int xSpeed, int ySpeed, int radius){
        center = new MovablePoint (x, y, xSpeed, ySpeed);
        this.radius = radius;

    }

    @Override
    public String toString() {
        return "MovableCircle{" +
                "radius=" + radius +
                ", center=" + center +
                '}';
    }
    @Override
    public void moveUp(){
        center.ySpeed++;
    }
    @Override
    public void moveDown(){
        center.ySpeed--;
    }
    @Override
    public void moveLeft(){
        center.xSpeed--;
    }
    @Override
    public void moveRight(){
        center.xSpeed++;
    }
}
