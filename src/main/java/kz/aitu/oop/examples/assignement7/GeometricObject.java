package kz.aitu.oop.examples.assignement7;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}
